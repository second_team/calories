from django.contrib import admin
from calories.models import Calorie


class CalorieAdmin(admin.ModelAdmin):
    list_display = ('user', 'text', 'calories', 'date', 'time')
    readonly_fields = ('user',)


admin.site.register(Calorie, CalorieAdmin)
