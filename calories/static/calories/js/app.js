window.CalorieManager={
    Models: {},
    Collections: {},
    Views: {},
    start: function() {
        var old_ajax = Backbone.ajax;
        Backbone.ajax = function(request) {
            if(request.headers == undefined) {
                request.headers = {};
            }
            request.headers['X-CSRFToken'] = $.cookie('csrftoken');
            return old_ajax(request);
        };

        var calories = new CalorieManager.Collections.Calories();

        console.log('CalorieManager started');

        var router = new CalorieManager.Router();

        router.on('route:home', function() {
            CalorieManager.Models.User.get_user(
                function (model, response, options) {
                    calories.update(function() {
                        router.navigate('calories', {
                            trigger: true,
                            replace: true
                        });
                    });
                }, function(model, response, options) {
                    router.navigate('login', {
                        trigger: true,
                        replace: true
                    });
                });
        });

        router.on('route:login', function() {
            console.log('Login');
            var loginForm = new CalorieManager.Views.LoginForm({
                model: new CalorieManager.Models.LoginUser()
            });

            loginForm.on('form:submitted', function(attrs) {
                loginForm.model.set(attrs);
                loginForm.model.set('username', attrs.login);
                if (loginForm.model.isValid(true)) {
                    loginForm.model.save(attrs, {
                        url: '/api/auth/login/',
                        success: function() {
                            router.navigate('', true);
                        },
                        error: function(model, response, options) {
                            loginForm.handle_server_error(model, response, options)
                        }

                    });
                }
            });
            $('#main-container').html(loginForm.render().$el);
        });
        router.on('route:logout', function() {
            $.ajax('/api/auth/logout/', {
                method: 'POST',
                dataType: 'json',
                headers: {
                    'X-CSRFToken': $.cookie('csrftoken')
                },
                success: function(res) {
                    console.log('Logout');
                    router.navigate('', {
                        trigger: true,
                        replace: true
                    });
                }
            });
        });

        router.on('route:signup', function() {
            console.log('Signup');

            var signupForm = new CalorieManager.Views.SignupForm({
                model: new CalorieManager.Models.NewUser()
            });

            signupForm.on('form:submitted', function(attrs) {
                signupForm.model.set(attrs);
                if (signupForm.model.isValid(true)) {
                    signupForm.model.save(attrs, {
                        url: '/api/auth/signup/',
                        success: function() {
                            router.navigate('login', true);
                        },
                        error: function(model, response, options) {
                            signupForm.handle_server_error(model, response, options)
                        }
                    });
                }
            });
            $('#main-container').html(signupForm.render().$el);
        });

        router.on('route:profile', function() {
            console.log('Profile');

            CalorieManager.Models.User.get_user(function(model, response, options) {
                var userForm = new CalorieManager.Views.UserForm({
                    model: model
                });
                userForm.on('form:submitted', function(attrs) {
                    userForm.model.set(attrs);
                    if (userForm.model.isValid(true)) {
                        userForm.model.save(attrs, {
                            url: '/api/user/',
                            method: 'PUT',
                            success: function() {
                                userForm.yield_about_success('Updated');
                                calories.update();
                                //router.navigate('profile', true);
                            },
                            error: function(model, response, options) {
                                userForm.handle_server_error(model, response, options)
                            }
                        });
                    }
                });
                $('#main-container').html(userForm.render().$el);

            }, function() {
                router.navigate('login', {
                    trigger: true,
                    replace: true
                });
            });
        });

        router.on('route:showCalories', function() {
            console.log('Show calories');
            if(! calories.initialized) {
                calories.update(function() {
                    var caloriesView = new CalorieManager.Views.Calories({
                        collection: calories
                    });
                    $('#main-container').html(caloriesView.render().$el);
                });
            } else {
                var caloriesView = new CalorieManager.Views.Calories({
                    collection: calories
                });
                $('#main-container').html(caloriesView.render().$el);
            }
        });

        router.on('route:newCalorie', function() {
            console.log('New calorie');
            var newCalorieForm = new CalorieManager.Views.CalorieForm({
                model: new CalorieManager.Models.Calorie()
            });
            Backbone.Validation.bind(newCalorieForm);

            newCalorieForm.on('form:submitted', function(attrs) {
                newCalorieForm.model.set(attrs);
                if (newCalorieForm.model.isValid(true)) {
                    calories.create(attrs, {
                        wait: true,
                        success: function() {
                            calories.update(function() {
                                router.navigate('calories', true);
                            });
                        }
                    });
                }
            });
            $('#main-container').html(newCalorieForm.render().$el);
            $('#calorie-text').focus();
        });

        router.on('route:editCalorie', function(id) {
            console.log('Edit calorie');

            var calorie = calories.get(id),
                editCaloriesForm;

            if (calorie) {
                editCaloriesForm = new CalorieManager.Views.CalorieForm({
                    model: calorie
                });
                editCaloriesForm.on('form:submitted', function(attrs) {
                    calorie.save(attrs, {
                        success: function() {
                            calories.update(function() {
                                router.navigate('calories', true);
                            });
                        }
                    });
                });

                $('#main-container').html(editCaloriesForm.render().$el);
            } else {
                router.navigate('calories', true);
            }
        });

        router.on('route:deleteCalorie', function(id) {
            console.log('Delete calorie');
            calories.get(id).destroy({success: function () {
                calories.update(function() {
                    router.navigate('calories', true);
                });
            }});
        });

        Backbone.history.start();
    }
};