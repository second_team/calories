CalorieManager.Views.BaseForm = Backbone.View.extend({
        initialize: function () {
            Backbone.Validation.bind(this);
        },
        clear_messages: function() {
            this.$el.find('.non-field-error').addClass('hidden');
            this.$el.find('.alert-success').addClass('hidden');
        },
        yield_about_success: function(msg) {
            this.$el.find('.alert-success').removeClass('hidden').html(msg);
        },
        cry_about_error: function(msg) {
            this.$el.find('.non-field-error').removeClass('hidden').html(msg);
        },
        handle_server_error: function (model, response, options) {
            if (response.status == 403) {
                // need relogin
                this.cry_about_error('You session is expired, please <a href="#login">re-login</a>.');
            }
            if (response.responseJSON != undefined) {
                var errors = response.responseJSON;
                for (var attr in this.model.attributes) {
                    if (attr in errors) {
                        for (var idx in errors[attr]) {
                            Backbone.Validation.callbacks.invalid(this, attr, errors[attr][idx]);
                        }
                    } else {
                        Backbone.Validation.callbacks.valid(this, attr);
                    }
                }
                if ('non_field_errors' in errors) {
                    for (var idx in errors['non_field_errors']) {
                        var errorBlock = $('<span>');
                        for (var j in errors['non_field_errors']) {
                            errorBlock
                                .append(errors['non_field_errors'][j])
                                .append('<br/>');
                        }
                        this.cry_about_error(errorBlock.html());
                    }
                }
            } else {
                this.cry_about_error(response.responseText);
            }
        }
    }
);

_.extend(Backbone.Validation.callbacks, {
    valid: function (view, attr, selector) {
        var $el = view.$('[name=' + attr + ']'),
            $group = $el.closest('.form-group');

        $group.removeClass('has-error');
        $group.find('.help-block').html('').addClass('hidden');
    },
    invalid: function (view, attr, error, selector) {
        var $el = view.$('[name=' + attr + ']'),
            $group = $el.closest('.form-group');

        $group.addClass('has-error');
        $group.find('.help-block').html(error).removeClass('hidden');
    }
});
