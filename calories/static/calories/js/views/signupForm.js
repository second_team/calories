CalorieManager.Views.SignupForm = CalorieManager.Views.BaseForm.extend({
    template: _.template($('#tpl-signup').html()),
    render: function() {
        var html = this.template(_.extend(this.model.toJSON(), {
            isNew: this.model.isNew()
        }));
        this.$el.append(html);
        return this;
    },
    events: {
        'submit #signup-form': 'onFormSubmit'
    },

    onFormSubmit: function(e) {
        e.preventDefault();

        var attrs = {
            username: this.$('#id_username').val(),
            password1: this.$('#id_password1').val(),
            password2: this.$('#id_password2').val(),
            email: this.$('#id_email').val()
        };
        this.trigger('form:submitted', attrs);
    }
});