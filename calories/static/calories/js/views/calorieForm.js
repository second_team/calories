CalorieManager.Views.CalorieForm = CalorieManager.Views.BaseForm.extend({
    template: _.template($('#tpl-calorie-form').html()),
    render: function() {
        var html = this.template(_.extend(this.model.toJSON(), {
            isNew: this.model.isNew()
        }));
        this.$el.append(html);
        return this;
    },
    events: {
        'submit #calorie-form': 'onFormSubmit'
    },

    onFormSubmit: function(e) {
        e.preventDefault();

        var attrs = {
            date: this.$('#id_date').val(),
            time: this.$('#id_time').val(),
            text: this.$('#id_text').val(),
            calories: this.$('#id_calories').val()
        };
        var id = this.$('#calorie-id').val();
        if (id) {
            attrs['id'] = id;
        }
        this.trigger('form:submitted', attrs);
    }
});