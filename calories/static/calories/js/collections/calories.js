CalorieManager.Collections.Calories = Backbone.Collection.extend({
    model: CalorieManager.Models.Calorie,
    url: '/api/calories/',
    initialized: false,

    update: function(callback) {
        var me = this;
        this.fetch({
            data: {
                date_from: Filters.date_from.get(''),
                date_to: Filters.date_to.get(''),
                time_from: Filters.time_from.get(''),
                time_to: Filters.time_to.get(''),
                order_column: Filters.order_column.get(''),
                order_dir: Filters.order_direction.get('')
            },
            success: function(collection, response, options) {
                me.reset(collection.models);
                me.initialized = true;
                if (callback != undefined) {
                    callback();
                }
            }
        });
    }
});