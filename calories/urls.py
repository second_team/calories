from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from calories.views import user, CalorieDetail

urlpatterns = [
    url(r'^$', 'calories.views.index'),
    url(r'^api/calories/$', 'calories.views.calorie_api', name='calorie-list'),
    url(r'^api/calories/(?P<pk>[0-9]+)/$', CalorieDetail.as_view(), name='calorie-detail'),
    url(r'^api/user/', user, name='user'),
    url(r'^api/auth/', include('rest_auth.urls')),
    url(r'^api/auth/signup/', include('rest_auth.registration.urls')),
    url(r'^api/accout/', include('allauth.account.urls')),
]
urlpatterns = format_suffix_patterns(urlpatterns)
