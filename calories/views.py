from django.contrib.auth.models import User
from django.shortcuts import render
from rest_auth.serializers import PasswordChangeSerializer
from rest_framework import filters, mixins
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.generics import RetrieveUpdateDestroyAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_405_METHOD_NOT_ALLOWED, HTTP_201_CREATED
from rest_framework.viewsets import ReadOnlyModelViewSet
from calories.forms import LoginForm, SignupForm, UserForm, CalorieForm, FilterForm
from calories.models import Calorie
from calories.permissions import IsMyUser, IsMe
from calories.serializers import CalorieSerializer, UserSerializer


class MyUserFilterBacked(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        return queryset.filter(user=request.user)


class UserFilterBackend(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        return queryset.filter(id=request.user.id)


@permission_classes((IsAuthenticated, IsMyUser, ))
@authentication_classes((SessionAuthentication, BasicAuthentication, ))
class CalorieDetail(RetrieveUpdateDestroyAPIView):
    queryset = Calorie.objects.all()
    serializer_class = CalorieSerializer


@api_view(['GET', 'POST'])
@permission_classes((IsAuthenticated, IsMe, ))
@authentication_classes((SessionAuthentication, BasicAuthentication, ))
def calorie_api(request):
    if request.method == 'GET':
        serializer = CalorieSerializer(Calorie.objects.user_calories_filtered_by(request.user, request.QUERY_PARAMS), many=True, context={'request': request})
        return Response(serializer.data)
    elif request.method == 'POST':
        serializer = CalorieSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=HTTP_201_CREATED)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class UserViewSet(ReadOnlyModelViewSet, mixins.UpdateModelMixin, mixins.DestroyModelMixin,):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = (UserFilterBackend, )
    permission_classes = (IsAuthenticated, IsMe, )
    authentication_classes = (SessionAuthentication, BasicAuthentication, )


@api_view(['GET', 'PUT'])
@permission_classes((IsAuthenticated, IsMe, ))
@authentication_classes((SessionAuthentication, BasicAuthentication, ))
def user(request):
    if request.method == 'GET':
        return Response(UserSerializer(request.user, many=False, context = {request: request}).data)
    elif request.method == 'PUT':
        serializer = UserSerializer(request.user, data=request.data)
        if serializer.is_valid():
            serializer.save()

            if len(request.DATA.get('new_password1', None)) > 0 or len(request.DATA.get('new_password2', None)) > 0:
                passwordChangeSerializer = PasswordChangeSerializer(context={'request': request, 'user': request.user, 'many': False}, data=request.data)
                if passwordChangeSerializer.is_valid():
                    passwordChangeSerializer.save()
                else:
                    return Response(passwordChangeSerializer.errors, status=HTTP_400_BAD_REQUEST)

            return Response(serializer.data)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
    else:
        return Response(status=HTTP_405_METHOD_NOT_ALLOWED)


def index(request):
    return render(request, "calories/index.html", {
        'login_form': LoginForm(),
        'signup_form': SignupForm(),
        'profile_form': UserForm(),
        'calorie_form': CalorieForm(),
        'filter_form': FilterForm()
    })
