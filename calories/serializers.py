from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.permissions import IsAuthenticated
from rest_framework.validators import UniqueTogetherValidator
from calories.models import Calorie, UserSettings
from calories.permissions import IsMyUser


class CalorieSerializer(serializers.ModelSerializer):
    permission_classes = (IsAuthenticated, IsMyUser,)

    class Meta:
        model = Calorie
        fields = ('url', 'id', 'date', 'time', 'text', 'calories', 'calories_alert')

    def build_unknown_field(self, field_name, model_class):
        if field_name == 'calories_alert':
            return self.build_property_field(field_name, model_class)
        return super(CalorieSerializer, self).build_unknown_field(field_name, model_class)


class UserSerializer(serializers.ModelSerializer):
    max_calories_per_day = serializers.IntegerField(source='settings.max_calories_per_day', default=0)
    permission_classes = (IsAuthenticated, )

    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name', 'max_calories_per_day')
        validators = [
            UniqueTogetherValidator(User.objects.all(), ('email',)),
        ]

    def create(self, validated_data):
        settings_data = validated_data.pop('settings', None)
        user = super(UserSerializer, self).create(validated_data)
        self.create_or_update_settings(user, settings_data)
        return user

    def update(self, instance, validated_data):
        settings_data = validated_data.pop('settings', None)
        self.create_or_update_settings(instance, settings_data)
        return super(UserSerializer, self).update(instance, validated_data)

    def create_or_update_settings(self, user, settings_data):
        settings, created = UserSettings.objects.get_or_create(user=user, defaults=settings_data)
        if not created and settings_data is not None:
            super(UserSerializer, self).update(settings, settings_data)

