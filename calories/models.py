from django.contrib.auth.models import User
from django.db import models


class CalorieManager(models.Manager):
    def user_calories_filtered_by(self, user, filters):
        where_clause = 'u.user_id = %s'
        where_params = [user.id, ]

        date_from = filters.get('date_from', None)
        date_to = filters.get('date_to', None)
        time_from = filters.get('time_from', None)
        time_to = filters.get('time_to', None)

        if date_from:
            where_clause += ' and c1.date >= %s'
            where_params += [date_from]
        if date_to:
            where_clause += ' and c1.date <= %s'
            where_params += [date_to]
        if time_from:
            where_clause += ' and c1.time >= %s'
            where_params += [time_from]
        if time_to:
            where_clause += ' and c1.time <= %s'
            where_params += [time_to]

        order_column = 0
        try:
            order_column = int(filters.get('order_column', 0))
        except ValueError:
            pass
        if order_column not in [0, 1, 2]:
            order_column = 0
        order_dir = filters.get('order_dir', 'asc')
        if order_dir not in ['asc', 'desc']:
            order_dir = 'asc'
        order_clause = ['c1.date {0}, c1.time {0}', 'c1.text {0}', 'c1.calories {0}'][order_column].format(order_dir)

        queryset = Calorie.objects.raw(
            "select c1.id, c1.date, c1.time, c1.text, c1.calories, u.max_calories_per_day-sum(c2.calories) < 0 as calories_alert "
            "from calories_calorie c1 "
            "left join calories_calorie c2 on c1.date=c2.date "
            "left join calories_usersettings u on c1.user_id = u.user_id "
            "where {0} "
            "group by c1.id "
            "order by {1} ".format(where_clause, order_clause)
            , where_params)
        return queryset


class Calorie(models.Model):
    objects = CalorieManager()

    user = models.ForeignKey(User, related_name='calories')
    date = models.DateField(db_index=True)
    time = models.TimeField(db_index=True)
    text = models.CharField(max_length=255)
    calories = models.PositiveIntegerField(db_index=True)

    class Meta:
        ordering = ('date','time')

    def jsonify(self):
        d = self.__dict__
        del d['_state']
        return d

    def __str__(self):
        return "{0.text} ({0.calories} cal) at {0.date} {0.time}".format(self)


class UserSettings(models.Model):
    user = models.OneToOneField(User)
    max_calories_per_day = models.PositiveIntegerField(default=0)

User.settings = property(lambda u: UserSettings.objects.get_or_create(user=u)[0])
