# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Calorie',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField()),
                ('time', models.TimeField()),
                ('text', models.CharField(max_length=255)),
                ('calories', models.PositiveIntegerField()),
                ('user', models.ForeignKey(related_name='calories', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-date', '-time'),
            },
            bases=(models.Model,),
        ),
    ]
