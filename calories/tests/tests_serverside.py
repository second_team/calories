from datetime import timedelta, datetime
from django.contrib.auth.models import User
from django.test import TestCase
from django.utils.timezone import now
from calories.models import UserSettings, Calorie
from calories.tests.const import MAX_CALORIES_PER_DAY, DATE_FORMAT, TEST_USERNAME1


class CalorieTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(TEST_USERNAME1)
        UserSettings(user=self.user, max_calories_per_day=MAX_CALORIES_PER_DAY).save()

    def tearDown(self):
        Calorie.objects.all().delete()

    def test_calorie_alert(self):
        today = now()

        # Eat an apple
        apple = Calorie(date=today, time=today, text='Apple', calories=100, user=self.user)
        apple.save()

        queryset = Calorie.objects.user_calories_filtered_by(self.user, {})
        calories = []
        for calorie in queryset:
            self.assertEqual(0, calorie.calories_alert, "Wrong alert") # no alert
            calories += [calorie]
        self.assertEqual([apple], calories, "Wrong calories list")

        today = today + timedelta(minutes=1)
        # eat big pie
        big_pie = Calorie(date=today, time=today, text='Big Pie', calories=MAX_CALORIES_PER_DAY+1, user=self.user)
        big_pie.save()

        queryset = Calorie.objects.user_calories_filtered_by(self.user, {})
        calories = []
        for calorie in queryset:
            self.assertEqual(1, calorie.calories_alert, "Wrong alert") # alert
            calories += [calorie]
        self.assertEqual([apple, big_pie], calories, "Wrong calories list")

        # increase alert border
        self.user.usersettings.max_calories_per_day = apple.calories + big_pie.calories
        self.user.usersettings.save()

        queryset = Calorie.objects.user_calories_filtered_by(self.user, {})
        calories = []
        for calorie in queryset:
            self.assertEqual(0, calorie.calories_alert, "Wrong alert") # no alert
            calories += [calorie]
        self.assertEqual(calories, [apple, big_pie], "Wrong calories list")

    def test_filtering(self):
        today_now = now()
        t1 = datetime(today_now.year, today_now.month, today_now.day, 5, 0, 0)
        t2 = datetime(today_now.year, today_now.month, today_now.day, 5, 20, 0)
        t3 = datetime(today_now.year, today_now.month, today_now.day, 7, 20, 0)
        yesterday = today_now - timedelta(days=1)
        y1 = datetime(yesterday.year, yesterday.month, yesterday.day, 5, 0, 0)
        y2 = datetime(yesterday.year, yesterday.month, yesterday.day, 6, 20, 0)
        y3 = datetime(yesterday.year, yesterday.month, yesterday.day, 8, 20, 0)
        day_before_yesterday = yesterday - timedelta(days=1)
        by1 = datetime(day_before_yesterday.year, day_before_yesterday.month, day_before_yesterday.day, 5, 0, 0)
        by2 = datetime(day_before_yesterday.year, day_before_yesterday.month, day_before_yesterday.day, 6, 20, 0)
        by3 = datetime(day_before_yesterday.year, day_before_yesterday.month, day_before_yesterday.day, 8, 20, 0)

        # Eat an apples
        a1 = Calorie(date=by1.date(), time=by1.time(), text='Apple', calories=100, user=self.user)
        a1.save()
        a2 = Calorie(date=by2.date(), time=by2.time(), text='Apple', calories=100, user=self.user)
        a2.save()
        a3 = Calorie(date=by3.date(), time=by3.time(), text='Apple', calories=100, user=self.user)
        a3.save()
        a4 = Calorie(date=y1.date(), time=y1.time(), text='Apple', calories=100, user=self.user)
        a4.save()
        a5 = Calorie(date=y2.date(), time=y2.time(), text='Apple', calories=100, user=self.user)
        a5.save()
        a6 = Calorie(date=y3.date(), time=y3.time(), text='Apple', calories=100, user=self.user)
        a6.save()
        a7 = Calorie(date=t1.date(), time=t1.time(), text='Apple', calories=100, user=self.user)
        a7.save()
        a8 = Calorie(date=t2.date(), time=t2.time(), text='Apple', calories=100, user=self.user)
        a8.save()
        a9 = Calorie(date=t3.date(), time=t3.time(), text='Apple', calories=100, user=self.user)
        a9.save()

        # no filter
        queryset = Calorie.objects.user_calories_filtered_by(self.user, {})
        calories = []
        for calorie in queryset:
            calories += [calorie]
        self.assertEqual([a1, a2, a3, a4, a5, a6, a7, a8, a9], calories, "Wrong unfiltered calories list")

        # date from filter
        queryset = Calorie.objects.user_calories_filtered_by(self.user, {'date_from': today_now.date().strftime(DATE_FORMAT)})
        calories = []
        for calorie in queryset:
            calories += [calorie]
        self.assertEqual([a7, a8, a9], calories, "Wrong calories list filtered date_from today")        # date from filter

        # date to filter
        queryset = Calorie.objects.user_calories_filtered_by(self.user, {'date_to': yesterday.date().strftime(DATE_FORMAT)})
        calories = []
        for calorie in queryset:
            calories += [calorie]
        self.assertEqual([a1, a2, a3, a4, a5, a6], calories, "Wrong calories list filtered date_to yesterday")

        # date from and to filter
        queryset = Calorie.objects.user_calories_filtered_by(self.user, {
            'date_from': day_before_yesterday.date().strftime(DATE_FORMAT),
            'date_to': day_before_yesterday.date().strftime(DATE_FORMAT)
        })
        calories = []
        for calorie in queryset:
            calories += [calorie]
        self.assertEqual([a1, a2, a3], calories, "Wrong calories list filtered date_from and date_to day before yeasterday")

        # time from filter
        queryset = Calorie.objects.user_calories_filtered_by(self.user, {
            'time_from': '06:00'
        })
        calories = []
        for calorie in queryset:
            calories += [calorie]
        self.assertEqual([a2, a3, a5, a6, a9], calories, "Wrong calories list filtered time_from 06:00")

        # time to filter
        queryset = Calorie.objects.user_calories_filtered_by(self.user, {
            'time_to': '07:00'
        })
        calories = []
        for calorie in queryset:
            calories += [calorie]
        self.assertEqual([a1, a2, a4, a5, a7, a8], calories, "Wrong calories list filtered time_to 07:00")

        # time from and to filter
        queryset = Calorie.objects.user_calories_filtered_by(self.user, {
            'time_from': '06:00',
            'time_to': '07:00'
        })
        calories = []
        for calorie in queryset:
            calories += [calorie]
        self.assertEqual([a2, a5], calories, "Wrong calories list filtered time_from 06:00 and time_to 07:00")

        # time from and to filter
        queryset = Calorie.objects.user_calories_filtered_by(self.user, {
            'time_from': '06:00',
            'time_to': '08:00'
        })
        calories = []
        for calorie in queryset:
            calories += [calorie]
        self.assertEqual([a2, a5, a9], calories, "Wrong calories list filtered time_from 06:00 and time_to 08:00")

        # time from filter
        queryset = Calorie.objects.user_calories_filtered_by(self.user, {
            'time_from': '12:00',
        })
        calories = []
        for calorie in queryset:
            calories += [calorie]
        self.assertEqual([], calories, "Wrong calories list filtered time_from 12:00")

        # date from and to and time from and to filter
        queryset = Calorie.objects.user_calories_filtered_by(self.user, {
            'date_from': yesterday.date().strftime(DATE_FORMAT),
            'date_to': yesterday.date().strftime(DATE_FORMAT),
            'time_from': '7:00',
            'time_to': '9:00'
        })
        calories = []
        for calorie in queryset:
            calories += [calorie]
        self.assertEqual([a6], calories, "Wrong calories list filtered date_from yesterday and date_to yeasterday and time_from 07:00 and time_to 09:00")

    def test_sorting(self):
        today_now = now()
        t1 = datetime(today_now.year, today_now.month, today_now.day, 5, 0, 0)
        t2 = datetime(today_now.year, today_now.month, today_now.day, 5, 20, 0)
        t3 = datetime(today_now.year, today_now.month, today_now.day, 7, 20, 0)

        a1 = Calorie(date=t1.date(), time=t1.time(), text='Apple', calories=100, user=self.user)
        a1.save()
        a2 = Calorie(date=t2.date(), time=t2.time(), text='Banana', calories=250, user=self.user)
        a2.save()
        a3 = Calorie(date=t3.date(), time=t3.time(), text='Nuts', calories=1000, user=self.user)
        a3.save()

        # default sorting
        queryset = Calorie.objects.user_calories_filtered_by(self.user, {})
        calories = []
        for calorie in queryset:
            calories += [calorie]
        self.assertEqual([a1, a2, a3], calories, "Wrong default sorting")

        # sorting datetime asc
        queryset = Calorie.objects.user_calories_filtered_by(self.user, {
            'order_column': 0,
            'order_dir': 'asc'
        })
        calories = []
        for calorie in queryset:
            calories += [calorie]
        self.assertEqual([a1, a2, a3], calories, "Wrong sorting datetime:asc")

        # sorting datetime desc
        queryset = Calorie.objects.user_calories_filtered_by(self.user, {
            'order_column': 0,
            'order_dir': 'desc'
        })
        calories = []
        for calorie in queryset:
            calories += [calorie]
        self.assertEqual([a3, a2, a1], calories, "Wrong sorting datetime:desc")

        # sorting text asc
        queryset = Calorie.objects.user_calories_filtered_by(self.user, {
            'order_column': 1,
            'order_dir': 'asc'
        })
        calories = []
        for calorie in queryset:
            calories += [calorie]
        self.assertEqual([a1, a2, a3], calories, "Wrong sorting text:asc")

        # sorting text desc
        queryset = Calorie.objects.user_calories_filtered_by(self.user, {
            'order_column': 1,
            'order_dir': 'desc'
        })
        calories = []
        for calorie in queryset:
            calories += [calorie]
        self.assertEqual([a3, a2, a1], calories, "Wrong sorting text:desc")

        # sorting calories asc
        queryset = Calorie.objects.user_calories_filtered_by(self.user, {
            'order_column': 2,
            'order_dir': 'asc'
        })
        calories = []
        for calorie in queryset:
            calories += [calorie]
        self.assertEqual([a1, a2, a3], calories, "Wrong sorting calories:asc")

        # sorting calories desc
        queryset = Calorie.objects.user_calories_filtered_by(self.user, {
            'order_column': 2,
            'order_dir': 'desc'
        })
        calories = []
        for calorie in queryset:
            calories += [calorie]
        self.assertEqual([a3, a2, a1], calories, "Wrong sorting calories:desc")


